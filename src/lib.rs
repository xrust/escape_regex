use regex::Regex;

pub fn interpolate(pattern: &str, delim_l: &str, delim_r: &str) -> String {
    let mut res = vec![];
    let l = regex::escape(delim_l);
    let r = regex::escape(delim_r);
    let re = format!("{l}.*?{r}");
    for token in split_keep(&Regex::new(&re).unwrap(), pattern) {
        if token.starts_with(delim_l) && token.ends_with(delim_r) {
            let token = token.strip_prefix(delim_l).unwrap();
            let token = token.strip_suffix(delim_r).unwrap();
            res.push(token.to_string())
        } else {
            res.push(regex::escape(token))
        }
    }
    res.join("")
}

fn split_keep<'a>(re: &Regex, text: &'a str) -> Vec<&'a str> {
    let mut result = Vec::new();
    let mut last = 0;
    for res in re.find_iter(text) {
        let index = res.start();
        let matched = res.as_str();
        if last != index {
            result.push(&text[last..index]);
        }
        result.push(matched);
        last = index + matched.len();
    }
    if last < text.len() {
        result.push(&text[last..]);
    }
    result
}

#[cfg(test)]
mod tests {
    use super::interpolate;

    #[test]
    fn test_interpolate() {
        assert_eq!(interpolate(r".<.*>.", "<", ">"), r"\..*\.");
    }
}
